<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B27611">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="_245">On the death of the illustrious George Duke of Albemarle</title>
    <author>M. D.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B27611 of text R43512 in the <ref target="B27611-http;//estc.bl.uk">English Short Title Catalog</ref>. Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in	 a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B27611</idno>
    <idno type="stc">Wing O310A</idno>
    <idno type="oclc">ocm 27875735</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this text, in whole or in part. Please contact project staff at eebotcp-info(at)umich.edu for further information or permissions.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online text creation partnership.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B27611)</note>
    <note>Transcribed from: (Early English Books Online ; image set 205766)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1731:12)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title type="_245">On the death of the illustrious George Duke of Albemarle</title>
      <author>M. D.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.).   </extent>
     <publicationStmt>
      <publisher>Printed for Sa. Heyrick ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>[1670?]</date>
     </publicationStmt>
     <notesStmt>
      <note>In verse.</note>
      <note>Signed: M.D.</note>
      <note>Text enclosed in mourning border.</note>
      <note>Reproduction of original in Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>Header created with script mrcb2eeboutf.xsl on 2016-01-28.</p>
   </projectDesc>
   <editorialDecl n="4">
    <p>Manually keyed and coded text linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
    <p>Issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
    <p>Keying and markup guidelines available at TCP web site (http://www.textcreationpartnership.org/docs/)</p>
   </editorialDecl>
  </encodingDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="lcsh">
     <term type="personal_name">Albemarle, George Monck, --  Duke of, 1608-1670 --  Poetry.</term>
     <term type="genre_form">Broadsides --  London (England) --  17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>On the death of the illustrious George Duke of Albemarle</ep:title>
    <ep:author>M. D.</ep:author>
    <ep:publicationYear>1670</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>408</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2014-05 </date>
    <label>SPi Global</label>Keyed and coded from ProQuest page images 
      </change>
   <change>
    <date>2014-06 </date>
    <label>Anne Simpson </label>Sampled and proofread 
      </change>
   <change>
    <date>2014-06 </date>
    <label>Anne Simpson</label>Text and markup reviewed and edited 
      </change>
   <change>
    <date>2015-03 </date>
    <label>pfs</label>Batch review (QC) and XML conversion 
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B27611-e10010" xml:lang="eng">
  <body xml:id="B27611-e10020">
   <div type="elegy" xml:id="B27611-e10030">
    <pb facs="1" xml:id="B27611-001-a"/>
    <head xml:id="B27611-e10040">
     <w lemma="on" pos="acp" xml:id="B27611-001-a-0010">On</w>
     <w lemma="the" pos="d" xml:id="B27611-001-a-0020">the</w>
     <w lemma="death" pos="n1" xml:id="B27611-001-a-0030">Death</w>
     <w lemma="of" pos="acp" xml:id="B27611-001-a-0040">of</w>
     <w lemma="the" pos="d" xml:id="B27611-001-a-0050">the</w>
     <w lemma="illustrious" pos="j" xml:id="B27611-001-a-0060">Illustrious</w>
     <w lemma="george" pos="nn1" xml:id="B27611-001-a-0070">GEORGE</w>
     <hi xml:id="B27611-e10050">
      <w lemma="duke" pos="n1" xml:id="B27611-001-a-0080">Duke</w>
      <w lemma="of" pos="acp" xml:id="B27611-001-a-0090">of</w>
     </hi>
     <w lemma="albemarle" pos="nn1" xml:id="B27611-001-a-0100">ALBEMARLE</w>
     <pc unit="sentence" xml:id="B27611-001-a-0110">.</pc>
    </head>
    <lg xml:id="B27611-e10060">
     <l xml:id="B27611-e10070">
      <w lemma="so" pos="av" xml:id="B27611-001-a-0120">SO</w>
      <w lemma="stoop" pos="vvz" xml:id="B27611-001-a-0130">stoops</w>
      <w lemma="our" pos="po" xml:id="B27611-001-a-0140">our</w>
      <w lemma="great" pos="j" xml:id="B27611-001-a-0150">great</w>
      <hi xml:id="B27611-e10080">
       <w lemma="alcides" pos="nn1" xml:id="B27611-001-a-0160">Alcides</w>
       <pc xml:id="B27611-001-a-0170">,</pc>
       <w lemma="monck" pos="nn1" reg="monk" xml:id="B27611-001-a-0180">MONCK</w>
       <pc xml:id="B27611-001-a-0190">,</pc>
      </hi>
      <w lemma="who" pos="crq" xml:id="B27611-001-a-0200">whose</w>
      <w lemma="brow" pos="n1" xml:id="B27611-001-a-0210">brow</w>
     </l>
     <l xml:id="B27611-e10090">
      <w lemma="will" pos="vmd" xml:id="B27611-001-a-0220">Would</w>
      <w lemma="never" pos="avx" reg="ne'er" xml:id="B27611-001-a-0230">ne'r</w>
      <w lemma="learn" pos="vvi" xml:id="B27611-001-a-0240">learn</w>
      <w lemma="what" pos="crq" xml:id="B27611-001-a-0250">what</w>
      <w lemma="submission" pos="n1" xml:id="B27611-001-a-0260">Submission</w>
      <w lemma="mean" pos="vvd" xml:id="B27611-001-a-0270">meant</w>
      <w lemma="till" pos="acp" xml:id="B27611-001-a-0280">till</w>
      <w lemma="now" pos="av" xml:id="B27611-001-a-0290">now</w>
      <pc xml:id="B27611-001-a-0300">:</pc>
     </l>
     <l xml:id="B27611-e10100">
      <w lemma="who" pos="crq" xml:id="B27611-001-a-0310">Whose</w>
      <w lemma="wise" pos="js" xml:id="B27611-001-a-0320">wisest</w>
      <w lemma="conduct" pos="vvb" xml:id="B27611-001-a-0330">Conduct</w>
      <pc xml:id="B27611-001-a-0340">,</pc>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-0350">and</w>
      <w lemma="who" pos="crq" xml:id="B27611-001-a-0360">whose</w>
      <w lemma="watchful" pos="j" xml:id="B27611-001-a-0370">watchful</w>
      <w lemma="care" pos="n1" xml:id="B27611-001-a-0380">Care</w>
      <pc xml:id="B27611-001-a-0390">,</pc>
     </l>
     <l xml:id="B27611-e10110">
      <w lemma="still" pos="av" xml:id="B27611-001-a-0400">Still</w>
      <w lemma="speak" pos="vvd" xml:id="B27611-001-a-0410">spake</w>
      <w lemma="he" pos="pno" xml:id="B27611-001-a-0420">him</w>
      <w lemma="great" pos="j" rend="hi" xml:id="B27611-001-a-0430">Great</w>
      <w lemma="in" pos="acp" xml:id="B27611-001-a-0440">in</w>
      <w lemma="the" pos="d" xml:id="B27611-001-a-0450">th'</w>
      <w lemma="bold" pos="j" xml:id="B27611-001-a-0460">bold</w>
      <w lemma="intrigue" pos="n2" xml:id="B27611-001-a-0470">Intrigues</w>
      <w lemma="of" pos="acp" xml:id="B27611-001-a-0480">of</w>
      <w lemma="war" pos="n1" xml:id="B27611-001-a-0490">War</w>
      <pc xml:id="B27611-001-a-0500">:</pc>
     </l>
     <l xml:id="B27611-e10120">
      <w lemma="who" pos="crq" xml:id="B27611-001-a-0510">Whose</w>
      <w lemma="mighty" pos="j" xml:id="B27611-001-a-0520">mighty</w>
      <w lemma="prowest" pos="n2" xml:id="B27611-001-a-0530">Prowests</w>
      <w lemma="trophy" pos="n2" xml:id="B27611-001-a-0540">Trophies</w>
      <w lemma="can" pos="vmd" xml:id="B27611-001-a-0550">could</w>
      <w lemma="afford" pos="vvi" xml:id="B27611-001-a-0560">afford</w>
     </l>
     <l xml:id="B27611-e10130">
      <w lemma="for" pos="acp" xml:id="B27611-001-a-0570">For</w>
      <w lemma="victory" pos="n2" xml:id="B27611-001-a-0580">Victories</w>
      <w lemma="still" pos="av" xml:id="B27611-001-a-0590">still</w>
      <w lemma="breathe" pos="vvg" xml:id="B27611-001-a-0600">breathing</w>
      <w lemma="from" pos="acp" xml:id="B27611-001-a-0610">from</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-0620">his</w>
      <w lemma="sword" pos="n1" xml:id="B27611-001-a-0630">Sword</w>
      <pc xml:id="B27611-001-a-0640">:</pc>
     </l>
     <l xml:id="B27611-e10140">
      <w lemma="who" pos="crq" xml:id="B27611-001-a-0650">Whose</w>
      <w lemma="fierce" pos="jc" xml:id="B27611-001-a-0660">fiercer</w>
      <w lemma="arm" pos="n1" xml:id="B27611-001-a-0670">Arm</w>
      <w lemma="breed" pos="vvn" reg="bread" xml:id="B27611-001-a-0680">bred</w>
      <w lemma="terror" pos="n1" reg="terror" xml:id="B27611-001-a-0690">Terrour</w>
      <w lemma="where" pos="crq" xml:id="B27611-001-a-0700">where</w>
      <w lemma="it" pos="pn" xml:id="B27611-001-a-0710">it</w>
      <w lemma="fight" pos="vvd" xml:id="B27611-001-a-0720">fought</w>
      <pc xml:id="B27611-001-a-0730">,</pc>
     </l>
     <l xml:id="B27611-e10150">
      <w lemma="and" pos="cc" xml:id="B27611-001-a-0740">And</w>
      <w lemma="every" pos="d" xml:id="B27611-001-a-0750">every</w>
      <w lemma="limb" pos="n1" xml:id="B27611-001-a-0760">Limb</w>
      <w lemma="he" pos="pns" xml:id="B27611-001-a-0770">he</w>
      <w lemma="wear" pos="vvd" xml:id="B27611-001-a-0780">wore</w>
      <w lemma="be" pos="vvd" xml:id="B27611-001-a-0790">was</w>
      <w lemma="castriot" pos="n1" rend="hi" xml:id="B27611-001-a-0800">Castriot</w>
      <pc unit="sentence" xml:id="B27611-001-a-0810">.</pc>
     </l>
    </lg>
    <lg xml:id="B27611-e10160">
     <l xml:id="B27611-e10170">
      <w lemma="thrice" pos="av" xml:id="B27611-001-a-0820">Thrice</w>
      <w lemma="great" pos="jc" xml:id="B27611-001-a-0830">greater</w>
      <w lemma="yet" pos="av" xml:id="B27611-001-a-0840">yet</w>
      <pc xml:id="B27611-001-a-0850">,</pc>
      <w lemma="since" pos="acp" xml:id="B27611-001-a-0860">since</w>
      <w lemma="george" pos="nn1" rend="hi" xml:id="B27611-001-a-0870">GEORGE</w>
      <w lemma="can" pos="vmd" xml:id="B27611-001-a-0880">could</w>
      <w lemma="overcome" pos="vvi" xml:id="B27611-001-a-0890">overcome</w>
      <pc xml:id="B27611-001-a-0900">,</pc>
     </l>
     <l xml:id="B27611-e10180">
      <w lemma="and" pos="cc" xml:id="B27611-001-a-0910">And</w>
      <w lemma="fix" pos="vvi" xml:id="B27611-001-a-0920">fix</w>
      <w lemma="a" pos="d" xml:id="B27611-001-a-0930">a</w>
      <w lemma="peace" pos="n1" xml:id="B27611-001-a-0940">Peace</w>
      <w lemma="from" pos="acp" xml:id="B27611-001-a-0950">from</w>
      <w lemma="civil" pos="j" xml:id="B27611-001-a-0960">Civil</w>
      <w lemma="war" pos="n2" xml:id="B27611-001-a-0970">Wars</w>
      <w lemma="at" pos="acp" xml:id="B27611-001-a-0980">at</w>
      <w lemma="home" pos="n1" xml:id="B27611-001-a-0990">home</w>
      <pc xml:id="B27611-001-a-1000">;</pc>
     </l>
     <l xml:id="B27611-e10190">
      <w lemma="a" pos="d" xml:id="B27611-001-a-1010">A</w>
      <w lemma="noble" pos="jc" xml:id="B27611-001-a-1020">nobler</w>
      <w lemma="way" pos="n1" xml:id="B27611-001-a-1030">way</w>
      <pc xml:id="B27611-001-a-1040">,</pc>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-1050">and</w>
      <w lemma="less" pos="avc-d" xml:id="B27611-001-a-1060">less</w>
      <w lemma="expensive" pos="j" xml:id="B27611-001-a-1070">expensive</w>
      <w lemma="too" pos="av" xml:id="B27611-001-a-1080">too</w>
      <pc xml:id="B27611-001-a-1090">,</pc>
     </l>
     <l xml:id="B27611-e10200">
      <w lemma="then" pos="av" xml:id="B27611-001-a-1100">Then</w>
      <w lemma="ere" pos="acp" xml:id="B27611-001-a-1110">ere</w>
      <w lemma="wise" pos="j" xml:id="B27611-001-a-1120">wise</w>
      <w lemma="homer" pos="nn1" rend="hi" xml:id="B27611-001-a-1130">Homer</w>
      <w lemma="make" pos="vvd" xml:id="B27611-001-a-1140">made</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-1150">his</w>
      <w lemma="hero" pos="n2" rend="hi" xml:id="B27611-001-a-1160">Heroes</w>
      <w lemma="do" pos="vvb" xml:id="B27611-001-a-1170">do</w>
      <pc xml:id="B27611-001-a-1180">:</pc>
     </l>
     <l xml:id="B27611-e10210">
      <w lemma="first" pos="ord" xml:id="B27611-001-a-1190">First</w>
      <w lemma="britain" pos="nng1" rend="hi" xml:id="B27611-001-a-1200">Britains</w>
      <w lemma="great" pos="js" xml:id="B27611-001-a-1210">greatest</w>
      <w lemma="tyranny" pos="n1" xml:id="B27611-001-a-1220">Tyranny</w>
      <w lemma="withstand" pos="vvn" xml:id="B27611-001-a-1230">withstood</w>
      <pc xml:id="B27611-001-a-1240">,</pc>
     </l>
     <l xml:id="B27611-e10220">
      <w lemma="then" pos="av" xml:id="B27611-001-a-1250">Then</w>
      <w lemma="teach" pos="vvd" xml:id="B27611-001-a-1260">taught</w>
      <w lemma="the" pos="d" xml:id="B27611-001-a-1270">the</w>
      <w lemma="way" pos="n1" xml:id="B27611-001-a-1280">way</w>
      <w lemma="to" pos="acp" xml:id="B27611-001-a-1290">to</w>
      <w lemma="conquest" pos="n1" xml:id="B27611-001-a-1300">Conquest</w>
      <w lemma="without" pos="acp" xml:id="B27611-001-a-1310">without</w>
      <w lemma="blood" pos="n1" reg="blood" xml:id="B27611-001-a-1320">Bloud</w>
      <pc xml:id="B27611-001-a-1330">;</pc>
     </l>
     <l xml:id="B27611-e10230">
      <w lemma="revive" pos="vvn" reg="revived" xml:id="B27611-001-a-1340">Reviv'd</w>
      <w lemma="a" pos="d" xml:id="B27611-001-a-1350">a</w>
      <w lemma="die" pos="j-vg" xml:id="B27611-001-a-1360">dying</w>
      <w lemma="sceptre" pos="n1" reg="sceptre" xml:id="B27611-001-a-1370">Scepter</w>
      <pc xml:id="B27611-001-a-1380">,</pc>
      <w lemma="save" pos="vvd" reg="saved" xml:id="B27611-001-a-1390">sav'd</w>
      <w lemma="a" pos="d" xml:id="B27611-001-a-1400">a</w>
      <w lemma="crown" pos="n1" xml:id="B27611-001-a-1410">Crown</w>
      <pc xml:id="B27611-001-a-1420">,</pc>
     </l>
     <l xml:id="B27611-e10240">
      <w lemma="call" pos="vvd" reg="called" xml:id="B27611-001-a-1430">Call'd</w>
      <w lemma="back" pos="av" xml:id="B27611-001-a-1440">back</w>
      <w lemma="great" pos="j" rend="hi" xml:id="B27611-001-a-1450">Great</w>
      <w lemma="charles" pos="nn1" xml:id="B27611-001-a-1460">CHARLES</w>
      <pc xml:id="B27611-001-a-1470">,</pc>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-1480">and</w>
      <w lemma="fix" pos="vvd" reg="fixed" xml:id="B27611-001-a-1490">fixt</w>
      <w lemma="he" pos="pno" xml:id="B27611-001-a-1500">him</w>
      <w lemma="in" pos="acp" xml:id="B27611-001-a-1510">in</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-1520">his</w>
      <w lemma="own" pos="d" xml:id="B27611-001-a-1530">own</w>
      <pc unit="sentence" xml:id="B27611-001-a-1540">.</pc>
     </l>
     <l xml:id="B27611-e10250">
      <w lemma="and" pos="cc" xml:id="B27611-001-a-1550">And</w>
      <w lemma="when" pos="crq" xml:id="B27611-001-a-1560">when</w>
      <w lemma="heaven" pos="n2" xml:id="B27611-001-a-1570">Heavens</w>
      <w lemma="agent'" pos="vvz" xml:id="B27611-001-a-1580">Agent's</w>
      <w lemma="strange" pos="js" xml:id="B27611-001-a-1590">strangest</w>
      <w lemma="work" pos="n1" xml:id="B27611-001-a-1600">work</w>
      <w lemma="be" pos="vvd" xml:id="B27611-001-a-1610">was</w>
      <w lemma="do" pos="vvn" xml:id="B27611-001-a-1620">done</w>
      <pc xml:id="B27611-001-a-1630">,</pc>
     </l>
     <l xml:id="B27611-e10260">
      <pc xml:id="B27611-001-a-1640">(</pc>
      <w lemma="by" pos="acp" xml:id="B27611-001-a-1650">By</w>
      <w lemma="that" pos="d" xml:id="B27611-001-a-1660">that</w>
      <w lemma="almighty" pos="j" xml:id="B27611-001-a-1670">Almighty</w>
      <w lemma="providence" pos="n1" xml:id="B27611-001-a-1680">Providence</w>
      <w lemma="begin" pos="vvn" xml:id="B27611-001-a-1690">begun</w>
      <pc xml:id="B27611-001-a-1700">)</pc>
     </l>
     <l xml:id="B27611-e10270">
      <w lemma="sit" pos="vvd" xml:id="B27611-001-a-1710">Sate</w>
      <w lemma="down" pos="acp" xml:id="B27611-001-a-1720">down</w>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-1730">and</w>
      <w lemma="kiss" pos="vvd" reg="kissed" xml:id="B27611-001-a-1740">kiss'd</w>
      <w lemma="reward" pos="n1" xml:id="B27611-001-a-1750">Reward</w>
      <w lemma="with" pos="acp" xml:id="B27611-001-a-1760">with</w>
      <w lemma="humble" pos="j" xml:id="B27611-001-a-1770">humble</w>
      <w lemma="sense" pos="n1" xml:id="B27611-001-a-1780">sense</w>
      <pc xml:id="B27611-001-a-1790">,</pc>
     </l>
     <l xml:id="B27611-e10280">
      <w lemma="shower" pos="vvn" reg="showered" xml:id="B27611-001-a-1800">Showr'd</w>
      <w lemma="on" pos="acp" xml:id="B27611-001-a-1810">on</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-1820">his</w>
      <w lemma="head" pos="n1" xml:id="B27611-001-a-1830">head</w>
      <w lemma="with" pos="acp" xml:id="B27611-001-a-1840">with</w>
      <w lemma="royal" pos="j" xml:id="B27611-001-a-1850">Royal</w>
      <w lemma="influence" pos="n1" xml:id="B27611-001-a-1860">Influence</w>
      <pc xml:id="B27611-001-a-1870">;</pc>
     </l>
     <l xml:id="B27611-e10290">
      <w lemma="where" pos="crq" xml:id="B27611-001-a-1880">Where</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-1890">his</w>
      <w lemma="wise" pos="j" xml:id="B27611-001-a-1900">wise</w>
      <w lemma="counsel" pos="n1" xml:id="B27611-001-a-1910">Counsel</w>
      <w lemma="show" pos="vvd" xml:id="B27611-001-a-1920">shew'd</w>
      <pc xml:id="B27611-001-a-1930">,</pc>
      <w lemma="his" pos="po" xml:id="B27611-001-a-1940">his</w>
      <w lemma="head" pos="n1" xml:id="B27611-001-a-1950">Head</w>
      <w lemma="can" pos="vmd" xml:id="B27611-001-a-1960">could</w>
      <w lemma="fare" pos="vvi" xml:id="B27611-001-a-1970">fare</w>
     </l>
     <l xml:id="B27611-e10300">
      <w lemma="as" pos="acp" xml:id="B27611-001-a-1980">As</w>
      <w lemma="well" pos="av" xml:id="B27611-001-a-1990">well</w>
      <w lemma="in" pos="acp" xml:id="B27611-001-a-2000">in</w>
      <w lemma="peace" pos="n1" xml:id="B27611-001-a-2010">Peace</w>
      <pc xml:id="B27611-001-a-2020">,</pc>
      <w lemma="as" pos="acp" xml:id="B27611-001-a-2030">as</w>
      <w lemma="once" pos="acp" xml:id="B27611-001-a-2040">once</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-2050">his</w>
      <w lemma="arm" pos="n2" xml:id="B27611-001-a-2060">Arms</w>
      <w lemma="in" pos="acp" xml:id="B27611-001-a-2070">in</w>
      <w lemma="war" pos="n1" xml:id="B27611-001-a-2080">War</w>
      <pc unit="sentence" xml:id="B27611-001-a-2090">.</pc>
     </l>
     <l xml:id="B27611-e10310">
      <w lemma="at" pos="acp" xml:id="B27611-001-a-2100">At</w>
      <w lemma="last" pos="ord" xml:id="B27611-001-a-2110">last</w>
      <pc xml:id="B27611-001-a-2120">,</pc>
      <pc xml:id="B27611-001-a-2130">(</pc>
      <w lemma="with" pos="acp" xml:id="B27611-001-a-2140">with</w>
      <w lemma="worth" pos="n1" xml:id="B27611-001-a-2150">Worth</w>
      <w lemma="still" pos="av" xml:id="B27611-001-a-2160">still</w>
      <w lemma="fraught" pos="vvn" xml:id="B27611-001-a-2170">fraught</w>
      <pc xml:id="B27611-001-a-2180">)</pc>
      <w lemma="take" pos="vvd" xml:id="B27611-001-a-2190">took</w>
      <w lemma="leave" pos="n1" xml:id="B27611-001-a-2200">leave</w>
      <pc xml:id="B27611-001-a-2210">,</pc>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-2220">and</w>
      <w lemma="so" pos="av" xml:id="B27611-001-a-2230">so</w>
      <pc xml:id="B27611-001-a-2240">,</pc>
     </l>
     <l xml:id="B27611-e10320">
      <w lemma="die" pos="vvd" reg="died" xml:id="B27611-001-a-2250">Dy'd</w>
      <w lemma="gracious" pos="j" xml:id="B27611-001-a-2260">Gracious</w>
      <w lemma="with" pos="acp" xml:id="B27611-001-a-2270">with</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-2280">his</w>
      <w lemma="king" pos="n1" xml:id="B27611-001-a-2290">King</w>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-2300">and</w>
      <w lemma="country" pos="n1" xml:id="B27611-001-a-2310">Country</w>
      <w lemma="too" pos="av" xml:id="B27611-001-a-2320">too</w>
      <pc xml:id="B27611-001-a-2330">;</pc>
     </l>
     <l xml:id="B27611-e10330">
      <w lemma="where" pos="crq" xml:id="B27611-001-a-2340">Where</w>
      <w lemma="now" pos="av" xml:id="B27611-001-a-2350">now</w>
      <w join="right" lemma="he" pos="pns" xml:id="B27611-001-a-2360">he</w>
      <w join="left" lemma="be" pos="vvz" xml:id="B27611-001-a-2361">'s</w>
      <w lemma="grow" pos="vvn" xml:id="B27611-001-a-2370">grown</w>
      <w lemma="immortal" pos="j" xml:id="B27611-001-a-2380">Immortal</w>
      <pc xml:id="B27611-001-a-2390">,</pc>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-2400">and</w>
      <w lemma="with" pos="acp" xml:id="B27611-001-a-2410">with</w>
      <w lemma="we" pos="pno" xml:id="B27611-001-a-2420">us</w>
     </l>
     <l xml:id="B27611-e10340">
      <w lemma="great" pos="jc" xml:id="B27611-001-a-2430">Greater</w>
      <pc xml:id="B27611-001-a-2440">,</pc>
      <w lemma="than" pos="cs" xml:id="B27611-001-a-2450">then</w>
      <w lemma="ere" pos="acp" xml:id="B27611-001-a-2460">ere</w>
      <w lemma="st.." pos="ab" xml:id="B27611-001-a-2470">St.</w>
      <w lemma="george" pos="nn1" rend="hi" xml:id="B27611-001-a-2480">George</w>
      <w lemma="of" pos="acp" xml:id="B27611-001-a-2490">of</w>
      <w lemma="cappadoce" pos="nn1" rend="hi" xml:id="B27611-001-a-2500">Cappadoce</w>
      <pc unit="sentence" xml:id="B27611-001-a-2510">.</pc>
     </l>
    </lg>
    <lg xml:id="B27611-e10350">
     <l xml:id="B27611-e10360">
      <w lemma="athens" pos="nn1" rend="hi" xml:id="B27611-001-a-2520">Athens</w>
      <w lemma="can" pos="vmd" xml:id="B27611-001-a-2530">could</w>
      <w lemma="never" pos="avx" xml:id="B27611-001-a-2540">never</w>
      <w lemma="boast" pos="vvi" xml:id="B27611-001-a-2550">boast</w>
      <w lemma="of" pos="acp" xml:id="B27611-001-a-2560">of</w>
      <w lemma="thing" pos="n2" xml:id="B27611-001-a-2570">things</w>
      <w lemma="like" pos="acp" xml:id="B27611-001-a-2580">like</w>
      <w lemma="these" pos="d" xml:id="B27611-001-a-2590">these</w>
      <pc unit="sentence" xml:id="B27611-001-a-2600">.</pc>
     </l>
     <l xml:id="B27611-e10370">
      <w lemma="though" pos="cs" xml:id="B27611-001-a-2610">Though</w>
      <w lemma="she" pos="pns" xml:id="B27611-001-a-2620">she</w>
      <w lemma="too" pos="av" xml:id="B27611-001-a-2630">too</w>
      <w lemma="have" pos="vvd" xml:id="B27611-001-a-2640">had</w>
      <w lemma="her" pos="po" xml:id="B27611-001-a-2650">her</w>
      <hi xml:id="B27611-e10380">
       <w lemma="great" pos="j" xml:id="B27611-001-a-2660">Great</w>
       <w lemma="themistocles" pos="nn1" xml:id="B27611-001-a-2670">Themistocles</w>
       <pc unit="sentence" xml:id="B27611-001-a-2680">.</pc>
      </hi>
     </l>
     <l xml:id="B27611-e10390">
      <w lemma="Rome" pos="nng1" rend="hi-apo-plain" xml:id="B27611-001-a-2700">Rome's</w>
      <w lemma="founder" pos="n1" xml:id="B27611-001-a-2710">Founder</w>
      <w lemma="be" pos="vvd" xml:id="B27611-001-a-2720">was</w>
      <w lemma="with" pos="ng1" xml:id="B27611-001-a-2730">with's</w>
      <w lemma="brother" pos="ng1" xml:id="B27611-001-a-2740">Brothers</w>
      <w lemma="blood" pos="n1" reg="blood" xml:id="B27611-001-a-2750">bloud</w>
      <w lemma="asperse" pos="vvn" reg="aspersed" xml:id="B27611-001-a-2760">asperst</w>
      <pc xml:id="B27611-001-a-2770">,</pc>
     </l>
     <l xml:id="B27611-e10400">
      <w lemma="which" orig="VVhich" pos="crq" xml:id="B27611-001-a-2780">Which</w>
      <w lemma="make" pos="vvd" xml:id="B27611-001-a-2790">made</w>
      <w lemma="their" pos="po" xml:id="B27611-001-a-2800">their</w>
      <w lemma="god" pos="n2" reg="gods" xml:id="B27611-001-a-2810">Godds</w>
      <w lemma="give" pos="vvb" xml:id="B27611-001-a-2820">give</w>
      <w lemma="out" pos="av" xml:id="B27611-001-a-2830">out</w>
      <w lemma="he" pos="pns" xml:id="B27611-001-a-2840">he</w>
      <w lemma="be" pos="vvd" xml:id="B27611-001-a-2850">was</w>
      <w lemma="accurse" pos="vvn" reg="accursed" xml:id="B27611-001-a-2860">accurst</w>
      <pc xml:id="B27611-001-a-2870">,</pc>
     </l>
     <l xml:id="B27611-e10410">
      <w lemma="though" pos="cs" xml:id="B27611-001-a-2880">Though</w>
      <w lemma="the" pos="d" xml:id="B27611-001-a-2890">the</w>
      <w lemma="blind" pos="j" reg="blind" xml:id="B27611-001-a-2900">blinde</w>
      <w lemma="common" pos="j" xml:id="B27611-001-a-2910">common</w>
      <w lemma="people" pos="n1" xml:id="B27611-001-a-2920">people</w>
      <w lemma="now" pos="av" xml:id="B27611-001-a-2930">now</w>
      <w lemma="think" pos="vvd" xml:id="B27611-001-a-2940">thought</w>
      <w lemma="most" pos="avs-d" xml:id="B27611-001-a-2950">most</w>
     </l>
     <l xml:id="B27611-e10420">
      <w lemma="their" pos="po" xml:id="B27611-001-a-2960">Their</w>
      <w lemma="king" pos="n1" xml:id="B27611-001-a-2970">King</w>
      <w lemma="be" pos="vvd" xml:id="B27611-001-a-2980">was</w>
      <w lemma="god" pos="nn1" reg="god" xml:id="B27611-001-a-2990">Godd</w>
      <pc xml:id="B27611-001-a-3000">,</pc>
      <w lemma="when" pos="crq" xml:id="B27611-001-a-3010">when</w>
      <w lemma="romulus" pos="nn1" rend="hi" xml:id="B27611-001-a-3020">Romulus</w>
      <w lemma="be" pos="vvd" xml:id="B27611-001-a-3030">was</w>
      <w lemma="lose" pos="vvn" xml:id="B27611-001-a-3040">lost</w>
      <pc unit="sentence" xml:id="B27611-001-a-3050">.</pc>
     </l>
     <l xml:id="B27611-e10430">
      <w lemma="their" pos="po" xml:id="B27611-001-a-3060">Their</w>
      <w lemma="good" pos="j" rend="hi" xml:id="B27611-001-a-3070">Good</w>
      <w lemma="Camillus" pos="nng1" rend="hi-apo-plain" xml:id="B27611-001-a-3090">Camillus's</w>
      <w lemma="triumph" pos="n1" xml:id="B27611-001-a-3100">Triumph</w>
      <w lemma="be" pos="vvd" xml:id="B27611-001-a-3110">was</w>
      <w lemma="too" pos="av" xml:id="B27611-001-a-3120">too</w>
      <w lemma="high" pos="j" xml:id="B27611-001-a-3130">high</w>
      <pc xml:id="B27611-001-a-3140">,</pc>
     </l>
     <l xml:id="B27611-e10440">
      <w lemma="and" pos="cc" xml:id="B27611-001-a-3150">And</w>
      <w lemma="coriolanus" pos="nn1" rend="hi" xml:id="B27611-001-a-3160">Coriolanus</w>
      <w lemma="die" pos="vvd" reg="died" xml:id="B27611-001-a-3170">dy'd</w>
      <w lemma="unhandsome" pos="av-j" xml:id="B27611-001-a-3180">unhandsomely</w>
      <pc unit="sentence" xml:id="B27611-001-a-3190">.</pc>
     </l>
     <l xml:id="B27611-e10450">
      <w join="right" lemma="it" pos="pn" xml:id="B27611-001-a-3200">'t</w>
      <w join="left" lemma="be" pos="vvd" xml:id="B27611-001-a-3201">was</w>
      <w lemma="only" pos="j" xml:id="B27611-001-a-3210">onely</w>
      <w lemma="george" pos="nn1" rend="hi" xml:id="B27611-001-a-3220">GEORGE</w>
      <w lemma="all" pos="d" xml:id="B27611-001-a-3230">all</w>
      <w lemma="wonder" pos="n1" xml:id="B27611-001-a-3240">Wonder</w>
      <w lemma="can" pos="vmd" xml:id="B27611-001-a-3250">could</w>
      <w lemma="put" pos="vvi" xml:id="B27611-001-a-3260">put</w>
      <w lemma="on" pos="acp" xml:id="B27611-001-a-3270">on</w>
      <pc xml:id="B27611-001-a-3280">,</pc>
     </l>
     <l xml:id="B27611-e10460">
      <w join="right" lemma="it" pos="pn" xml:id="B27611-001-a-3290">'t</w>
      <w join="left" lemma="be" pos="vvd" xml:id="B27611-001-a-3291">was</w>
      <w lemma="only" pos="j" xml:id="B27611-001-a-3300">onely</w>
      <w lemma="george" pos="nn1" rend="hi" xml:id="B27611-001-a-3310">GEORGE</w>
      <w lemma="can" pos="vmd" xml:id="B27611-001-a-3320">could</w>
      <w lemma="grasp" pos="vvi" xml:id="B27611-001-a-3330">grasp</w>
      <w lemma="perfection" pos="n1" xml:id="B27611-001-a-3340">Perfection</w>
      <pc unit="sentence" xml:id="B27611-001-a-3350">.</pc>
     </l>
     <l xml:id="B27611-e10470">
      <w lemma="ungrateful" pos="j" xml:id="B27611-001-a-3360">Ungrateful</w>
      <w lemma="then" pos="av" xml:id="B27611-001-a-3370">then</w>
      <pc xml:id="B27611-001-a-3380">,</pc>
      <w lemma="if" pos="cs" xml:id="B27611-001-a-3390">if</w>
      <w lemma="we" pos="pns" xml:id="B27611-001-a-3400">we</w>
      <w lemma="not" pos="xx" xml:id="B27611-001-a-3410">no</w>
      <w lemma="tear" pos="n2" xml:id="B27611-001-a-3420">Tears</w>
      <w lemma="allow" pos="vvi" xml:id="B27611-001-a-3430">allow</w>
     </l>
     <l xml:id="B27611-e10480">
      <w lemma="to" pos="acp" xml:id="B27611-001-a-3440">To</w>
      <w lemma="he" pos="pno" xml:id="B27611-001-a-3450">him</w>
      <w lemma="that" pos="cs" xml:id="B27611-001-a-3460">that</w>
      <w lemma="give" pos="vvd" xml:id="B27611-001-a-3470">gave</w>
      <w lemma="we" pos="pno" xml:id="B27611-001-a-3480">us</w>
      <w lemma="peace" pos="n1" xml:id="B27611-001-a-3490">Peace</w>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-3500">and</w>
      <w lemma="freedom" pos="n1" xml:id="B27611-001-a-3510">Freedom</w>
      <w lemma="too" pos="av" xml:id="B27611-001-a-3520">too</w>
      <pc xml:id="B27611-001-a-3530">:</pc>
     </l>
     <l xml:id="B27611-e10490">
      <w lemma="let" pos="vvb" xml:id="B27611-001-a-3540">Let</w>
      <w lemma="all" pos="d" xml:id="B27611-001-a-3550">all</w>
      <w lemma="with" pos="acp" xml:id="B27611-001-a-3560">with</w>
      <w lemma="sable" pos="jnn" xml:id="B27611-001-a-3570">Sable</w>
      <w lemma="colour" pos="n2" xml:id="B27611-001-a-3580">Colours</w>
      <w lemma="in" pos="acp" xml:id="B27611-001-a-3590">in</w>
      <w lemma="our" pos="po" xml:id="B27611-001-a-3600">our</w>
      <w lemma="isle" pos="n1" xml:id="B27611-001-a-3610">Isle</w>
     </l>
     <l xml:id="B27611-e10500">
      <w lemma="fall" pos="vvi" xml:id="B27611-001-a-3620">Fall</w>
      <w lemma="down" pos="acp" xml:id="B27611-001-a-3630">down</w>
      <pc xml:id="B27611-001-a-3640">,</pc>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-3650">and</w>
      <w lemma="spend" pos="vvi" xml:id="B27611-001-a-3660">spend</w>
      <w lemma="a" pos="d" xml:id="B27611-001-a-3670">a</w>
      <w lemma="sigh" pos="n1" xml:id="B27611-001-a-3680">sigh</w>
      <w join="right" lemma="at" pos="acp" xml:id="B27611-001-a-3690">at</w>
      <w join="left" lemma="his" pos="po" xml:id="B27611-001-a-3691">'s</w>
      <w lemma="funeral-pile" pos="n1" xml:id="B27611-001-a-3700">Funeral-pile</w>
      <pc unit="sentence" xml:id="B27611-001-a-3710">.</pc>
     </l>
     <l xml:id="B27611-e10510">
      <w lemma="may" pos="vmb" xml:id="B27611-001-a-3720">May</w>
      <w lemma="his" pos="po" xml:id="B27611-001-a-3730">his</w>
      <hi xml:id="B27611-e10520">
       <w lemma="great" pos="j" xml:id="B27611-001-a-3740">Great</w>
       <w lemma="story" pos="n1" xml:id="B27611-001-a-3750">Story</w>
      </hi>
      <w lemma="last" pos="ord" xml:id="B27611-001-a-3760">last</w>
      <pc xml:id="B27611-001-a-3770">,</pc>
      <w lemma="may" pos="vmb" xml:id="B27611-001-a-3780">may</w>
      <w lemma="GEORGE" pos="nng1" rend="hi-apo-plain" xml:id="B27611-001-a-3800">GEORGE's</w>
      <w lemma="name" pos="n1" xml:id="B27611-001-a-3810">Name</w>
     </l>
     <l xml:id="B27611-e10530">
      <w lemma="live" pos="vvb" xml:id="B27611-001-a-3820">Live</w>
      <w lemma="long" pos="av-j" xml:id="B27611-001-a-3830">long</w>
      <pc xml:id="B27611-001-a-3840">,</pc>
      <w lemma="and" pos="cc" xml:id="B27611-001-a-3850">and</w>
      <w lemma="flourish" pos="vvi" xml:id="B27611-001-a-3860">flourish</w>
      <w lemma="by" pos="acp" xml:id="B27611-001-a-3870">by</w>
      <w lemma="the" pos="d" xml:id="B27611-001-a-3880">the</w>
      <w lemma="mouth" pos="n1" xml:id="B27611-001-a-3890">mouth</w>
      <w lemma="of" pos="acp" xml:id="B27611-001-a-3900">of</w>
      <w lemma="fame" pos="n1" reg="famed" rend="hi" xml:id="B27611-001-a-3910">Fame</w>
      <pc unit="sentence" xml:id="B27611-001-a-3920">.</pc>
     </l>
    </lg>
    <closer xml:id="B27611-e10540">
     <signed xml:id="B27611-e10550">
      <w lemma="m." pos="ab" xml:id="B27611-001-a-3930">M.</w>
      <w lemma="duke" pos="n1" xml:id="B27611-001-a-3940">D.</w>
      <pc unit="sentence" xml:id="B27611-001-a-3950"/>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="B27611-e10560">
   <div type="colophon" xml:id="B27611-e10570">
    <p xml:id="B27611-e10580">
     <w lemma="london" pos="nn1" rend="hi" xml:id="B27611-001-a-3960">London</w>
     <pc xml:id="B27611-001-a-3970">,</pc>
     <w lemma="print" pos="vvn" xml:id="B27611-001-a-3980">Printed</w>
     <w lemma="for" pos="acp" xml:id="B27611-001-a-3990">for</w>
     <hi xml:id="B27611-e10590">
      <w lemma="sa" pos="uh" xml:id="B27611-001-a-4000">Sa</w>
      <pc xml:id="B27611-001-a-4010">:</pc>
      <w lemma="heyrick" pos="nn1" xml:id="B27611-001-a-4020">Heyrick</w>
     </hi>
     <w lemma="at" pos="acp" xml:id="B27611-001-a-4030">at</w>
     <w lemma="Grays-Inne-gate" pos="n1" xml:id="B27611-001-a-4050">Grays-Inne-gate</w>
     <pc xml:id="B27611-001-a-4060">,</pc>
     <w lemma="holborn" pos="nn1" xml:id="B27611-001-a-4070">Holborn</w>
     <pc unit="sentence" xml:id="B27611-001-a-4080">.</pc>
    </p>
   </div>
  </back>
 </text>
</TEI>
